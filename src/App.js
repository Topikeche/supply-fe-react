import React, { Component } from 'react';
// import 'materialize-css/dist/css/materialize.min.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Overview from './components/Overview';
import './App.css';
import Sidenav from './components/Sidenav';
import Header from './components/Header';
import Footer from './components/Footer';
import ProductList from './components/dashboard/product/ProductList'
import TransactionList from './components/dashboard/transaction/TransactionList'
import SignIn from './components/public/SignIn';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <div>
            <Header/>
            <Sidenav/>
            <main>
              <Switch>
                <Route exact path="/" component={ SignIn } />
                <Route exact path="/dashboard" component={ Overview } />
                <Route path="/dashboard/product" component={ ProductList } />
                <Route path="/dashboard/transaction" component={ TransactionList } />
              </Switch>
            </main>
            <Footer/>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
