import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import M from 'materialize-css'
import avatar from '../images/avatar.png'
import sidenavBg from '../images/sidenav-bg.jpg'

class Sidenav extends Component {

	state = {}

	componentDidMount = () => {
		document.addEventListener('DOMContentLoaded', function() {
			const options = {
				edge: 'left',
				draggable: true
			}
			var elems = document.querySelector('.sidenav');
			M.Sidenav.init(elems, options);		
			
			var elemCollaps = document.querySelector('.collapsible');
			var instances = M.Collapsible.init(elemCollaps, {});
			console.log(instances)
		})
	}

	closeSideNav = () => {

	}

	render() {
		return (
				<ul id="slide-out" className="sidenav sidenav-fixed z-depth-3">
					<li><div className="user-view">
						<div className="background">
							<img src={ sidenavBg } alt="Background Box" />
						</div>
						<a href="#user"><img className="circle" src={ avatar } alt="Profile User" /></a>
						<a href="#name"><span className="white-text name">John Doe</span></a>
						<a href="#email"><span className="white-text email">jdandturk@gmail.com</span></a>
					</div></li>
					<li><NavLink to="/dashboard/product"><i className="material-icons">local_mall</i>Produk</NavLink></li>
					<li><NavLink to="/dashboard/transaction"><i className="material-icons">swap_vertical_circle</i>Transaksi</NavLink></li>
					<li>
						<ul className="collapsible collapsible-accordion">
							<li>
								<a href="#!" className="collapsible-header">Dropdown<i className="material-icons right">arrow_drop_down</i></a>
								<div className="collapsible-body">
									<ul>
										<li><a href="#!">First</a></li>
										<li><a href="#!">Second</a></li>
										<li><a href="#!">Third</a></li>
										<li><a href="#!">Fourth</a></li>
									</ul>
								</div>
							</li>
						</ul>
					</li>
					<li><NavLink to="/dashboard/product"><i className="material-icons">local_mall</i>Produk</NavLink></li>
				</ul>
		)
	}

}

export default Sidenav
