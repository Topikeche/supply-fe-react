import React, { Component } from 'react'
import ProductSingle from './ProductSingle';

class ProductList extends Component {

	state = {
		products: [
			{id: 1, name: 'Indomie Goreng: Original', priceBuy: 2050, priceSell: 2500, stock: 25 },
			{id: 2, name: 'Indomie Goreng Keriting: Ayam Panggang', priceBuy: 4050, priceSell: 4500, stock: 8 },
			{id: 3, name: 'Indomie Goreng Jumbo: Ayam Panggang', priceBuy: 3150, priceSell: 3600, stock: 13 }
		]
	}

	render() {
		console.log(this.state)
		let productList = this.state.products.map(a => {
			return(
				<ProductSingle data={ a } key={ a.id }/>
			)
		})
		return (
			<div className="row">
				{ productList }
			</div>
		)
	}

}

export default ProductList
