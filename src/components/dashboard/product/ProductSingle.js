import React from 'react'

const ProductSingle = ({ data }) => {
	return (
		<div className="col s12 m4 l4 xl3">
			<div className="card">
				<div className="card-image waves-effect waves-block waves-light">
					<img className="activator" src="http://cdn.elevenia.co.id/g/6/1/2/3/7/2/21612372_B_V1.jpg" als="Product" alt="gambar produk" />
				</div>
				<div className="card-content">
					<span className="card-title activator grey-text text-darken-4">{ data.name }</span>
					<p><a href="#!">This is a link</a></p>
				</div>
				<div className="card-reveal">
					<span className="card-title grey-text text-darken-4">Card Title<i className="material-icons right">close</i></span>
					<p>Here is some more information about this product that is only revealed once clicked on.</p>
				</div>
			</div>
		</div>
	)
}

export default ProductSingle
