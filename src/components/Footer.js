import React from 'react'

const Footer = () => {
	return (
		<footer className="page-footer red darken-4">
      <div className="footer-copyright">
        © 2018 Copyright Text
        <i className="material-icons">dashboard</i>
      </div>
    </footer>
	)
}

export default Footer
